import 'bootstrap'
import Vue from 'vue'
import App from './App.vue'
import VueSocketIO from 'vue-socket.io';
import 'bootstrap/dist/css/bootstrap.min.css'

Vue.config.productionTip = false
Vue.use(new VueSocketIO({
  debug: true,
  connection: 'http://PRIVATE_IP:5000',
  vuex: {
      actionPrefix: 'SOCKET_',
      mutationPrefix: 'SOCKET_'
  },
}))

new Vue({
  render: h => h(App),
}).$mount('#app')
